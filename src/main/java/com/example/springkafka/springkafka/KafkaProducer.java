package com.example.springkafka.springkafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class KafkaProducer {
    private static final String TOPIC="my_topic";

    @Autowired
    private KafkaTemplate<String,String> kafkaTemplate;

    public void sendMessage() {
        UUID uuid = UUID.randomUUID();
        String randomString = uuid.toString();
        this.kafkaTemplate.send(TOPIC,randomString);
    }
}
