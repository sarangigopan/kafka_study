package com.example.springkafka.springkafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

@Component
@SpringBootApplication
public class SpringkafkaApplication implements CommandLineRunner {
	@Autowired
	private KafkaProducer kafkaProducer;
	public static void main(String[] args) {
		SpringApplication.run(SpringkafkaApplication.class, args);

	}
	@Override
	public void run(String...args) throws Exception {
		kafkaProducer.sendMessage();
	}

}
